# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
#word_lengths('hi mom') = > words = {'hi' => 2, 'mom' => 3}
#*itrate through the string and assign each string as the key and length as teh value

#create empty hash
#iterate through string
#make each string a key and assign it that strings length
#return hash
def word_lengths(str)
  words = {}
  str_array = str.split(' ')
  str_array.each { |word| words[word] = word.length }
  words
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
#greatest_key_by_val({'hi' => 2, 'mom' => 3}) => 'mom'
#*itereate through the hash and check for highest value, when found return key*

#create highest value variable
#iterate through hash
#save the first key in variable
#if not first key compare against variable and save if larger
#return variable
def greatest_key_by_val(hash)
  hash.sort_by { |k,v| v }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result.
#march = {rubies: 10, emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
#update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,moonstones: 5}
#merge the two hashes together and return the older
def update_inventory(older, newer)
  older.merge!(newer)
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
#letter_counts('mommy') => {'m' => 3, 'o' => 1, 'y' => 1}
#*iterate through string and count how many times each letter occurs*
def letter_counts(word)
  word_array = word.split('')
  ch_count = Hash.new(0)

  word_array.each { |el| ch_count[el] += 1 }

  ch_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
# my_uniq([1,1,2,3,4,4,4]) => [1,2,3,4]
#*save each item in array as a key, set all to equal 0, return an array of the keys*
def my_uniq(arr)
  arr_hash = Hash.new(0)

  arr.each { |num| arr_hash[num] = 0 }

  arr_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
#evens_and_odds([1,2,2,3,6,7,10]) => {evens: 4, odds: 3}
#create a new hash defaulted at zero
#itereate through array
#if number is even
#add one to evens
#else add one to odds
#return the hash
def evens_and_odds(numbers)
  number_count = Hash.new(0)

  numbers.each do |num|
    if num.odd?
      number_count[:odd] += 1
    else
      number_count[:even] += 1
    end
  end

  number_count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
#most_common_vowel('hi my mother said yo') => 'i'
#*create counter hash, only include in hash if vowel, return the key of highest val, if tied return vowel of lesser value*
def most_common_vowel(string)
  vowel_counter = Hash.new(0)
  vowels = ['a','e','i','o','u']
  str_array = string.split('')

  str_array.each do |ch|
    if vowels.include?(ch)
      vowel_counter[ch] += 1
    end
  end

  vowel_array = vowel_counter.sort_by { |k,v| v}
  vowel_array[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
#*check if values are greater than 6, if so save to an array, make a helper method that craetes all the different pairs *
#the helper method will iterate through the list and match with each proceeding name
def fall_and_winter_birthdays(students)
  found_students = students.select do |student, month|
    month >= 7
end
  names = found_students.keys
  all_pairs = []

    names.each_index do |idx|
      ((idx + 1)...names.length).each do |idx2|
        all_pairs << [names[idx], names[idx2]]
      end
    end

  all_pairs
end
# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  uniq_specimens = specimens.uniq
  species_count = {}

  uniq_specimens.each do |species|
    species_count[species] = specimens.count(species)
  end

  number_of_species = uniq_specimens.length
  smallest_species = species_count.values.min
  largest_species = species_count.values.max

  number_of_species **2 * smallest_species / largest_species
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandalized_count = character_count(vandalized_sign)

  vandalized_count.all? do |ch, count|
    normal_count[ch.downcase] >= count
  end
end

def character_count(str)
  count = Hash.new(0)

  str.each_char do |ch|
    next if ch == ' '
    count[ch.downcase] += 1
  end

  count
end
